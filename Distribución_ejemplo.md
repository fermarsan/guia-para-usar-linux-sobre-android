# Distribución de ejemplo

Esta guía muestra la instalación y configuración de una de las distribuciones diponibles en _Andronix-Termux_ así como su puesta en marcha y configuración básica. La idea es obtener una distribución con un entorno de escritorio desplegable a travez de un servidor _VNC_ que se ajuste a la resolución del celular para poder ser comodamente usada. Se ha se seleccionado para este ejemplo la distribución _Debian_ con el entorno de escitorio _LXDE_.

<div align="center">
<img src="/images/debian.jpeg" width="33%" />
<img src="/images/LXDE.jpeg" width="33%" />
</div

Para la instalación de la distribución con su escritorio, estos se debe seleccionar del menú de _Andronix_ y copiar el comando de instalación en _Termux_ como ya se vió en la guía general: [Entornos gráficos de Linux sobre Android](README.md).

Una vez instalada podemos ejecutar la distribución ejecutando en termux el script `start_debian` que esta en la carpeta home de _Termux_:

```
./start_debian.sh
```

<div align="center">
<img src="/images/termux-ls2.jpeg" width="33%" />
<img src="/images/start_debian.jpeg" width="33%" />
</div

Se debe visalizar en la consola de comandos `root@localhost` para asegurarnos que ya estamos dentro de la distribución.

## Configuración del VNC

Ya teniendo instalado el editor de texto _nano_ (`apt install nano`), editamos el script `vncserver-start`con _nano_:

```
nano /usr/local/bin/vncserver-start
```

Se modifica la geometría para que se ajuste al tamaño de pantalla del celular. En este caso se deja un poco más pequeña para lidiar con la forma redondeada de las esquinas del celular.

```
vncserver -name remote-desktop -geometry 720x1400 -localhost no :1
```

Ejecutamos en _Termux_ el comando `vncserver-start` y conectamos la sesión de _VNC Viewer_ a la direción `localhost:1`. Se debe redordar siempre la contraseña aue se definío en la instalación para el servidor VNC.

<div align="center">
<img src="/images/LXDE_maxres.jpeg" width="33%" />
</div

## Uso de _VNC Viewer_

Por defecto la calidad de las imágenes del visualizador VNC se ajusta de forma automática. Lo mejor es dejarlo configurado a calidad máxima, entrando al icono de información de la sesión VNC **i**, cambiar la opción `Picture quality`.

<div align="center">
<img src="/images/vnc-pic-quality.jpeg" width="33%" />
</div

Al conectarse desde _VNC Viewer_ al servidor VNC de nuestra distribución, podemos usar el cursor del "mouse" utilizando la pantalla táctil, así mismo podemos abrir un teclado en pantalla para digitar el texto que necesitemos. Todas esta opciones se muestran desplegando el menú de _VNC Viewer_ deslizando la parte superior de la pantalla hacia abajo:

<div align="center">
<img src="/images/vnc-menu.jpeg" width="33%" />
</div

## Configuración del escritorio _LXDE_

Para trabajar mas comodamente con el entorno de escritorio _LXDE_ sobre _ANdroid_ se sujiere realizar las siguientes configuraciones:

1. Cambiar la barra de menú a la parte superior. Esto hace mas comodo trabajar ya que el teclado en pantalla de _VNC Viewer_ siempre se despliega en la parte inferior de la pantalla.

2. Cambiar el tamaño de la barra de menú así como la de los iconos.

3. Dejar solo iconos en la barra de menú.

4. Deshabilitar algunos accesos directos de poco uso en la barra de menú. 


# Referencias
- [How to install an OS with Andronix? ](https://docs.andronix.app/unmodded-distros/unmodded-os-installation)
- [ VNC Basics](https://docs.andronix.app/vnc/vnc-basics/)

#### [<-Regresar](README.md)

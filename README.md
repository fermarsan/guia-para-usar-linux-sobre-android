# Guía para usar entornos gráficos de Linux sobre Android

Guía para usar Linux sobre Android haciendo uso de las aplicaciones [Termux](https://termux.com/), [AndroNix](https://andronix.app/) y [VNC Viewer](https://www.realvnc.com/es/connect/download/viewer/).

## Termux

Termux es un emulador de terminal para Android que permite ejecutar comandos e instalar aplicaciones de GNU/Linux.

Termux está disponible directamente desde el _Google Play Store_, pero para efectos de esta guía es necesario descargar la última versión desde la tienda de aplicaciones de software libre para Android **_F-Droid_**.

#### F-Droid

Para instalar _F-Droid_ siga las indicaciones de estos enlaces externos:

- [ ] [Link de descarga](https://f-droid.org/)
- [ ] [Introducción y guía de instalación](https://geekland.eu/instalar-tienda-aplicaciones-f-droid/)

Una vez instalado _F-Droid_ se busca Termux y se instala la última versión.

<!-- ![](/images/f-droid-termux.jpeg) -->
<div align="center">
<img src="/images/f-droid-termux.jpeg" width="33%" />
</div>


Termux por si mismo puede ejecutar [aplicaciones gráficas](https://wiki.termux.com/wiki/Graphical_Environment) por medio de un servidor VNC (Virtual Network Computing) como VNC Viewer. Termux puede ejecutar aplicaciones independientes o por medio de manejadores de ventanas como **Openbox** o **Fluxbox**, o entornos de escritorio como **LXQt**, **XFCE** o **MATE**. En el caso de esta guía se usará _Andronix_ para poder instalar distribuciones completas de GNU/Linux con más opciones de manejadores de ventanas y entornos de escritorio.


## AndroNix

AndroNix es una aplicación que permite instalar distribuciones de GNU/Linux acondicionadas para correr bajo Termux, esta se puede descargar del _Google Play Store_.

## Instalación de una distribución GNU/Linux usando AndroNix y Termux

Una vez instalada la ejecutamos y nos muestra el siguiente menú:

<div align="center">
<img src="/images/andronix-menu.jpeg" width="33%" />
</div>

Donde podemos escojer por medio de sus íconos entre las diferentes distribuciones disponibles: _Ububtu_, _Kali Linux_, _Debian_, _Arch Linux_, _Manjaro_, _Fedora_, _Void_ y _Alpine_.

Al entrar en alguno de ellos como por ejemplo _Debian_ se muestra la versión disponible y su descripción. Aquí debemos pulsar el botón `Proceed`, y se nos mostrará las opciones de instalar o desinstalar:

<div align="center">
<img src="/images/andronix-install.jpeg" width="33%" />
</div>

Luego se debe escojer entre 3 opciones de instalación: Con entorno de escritorio, con manejador de ventanas o con solo la interfaz de comandos (sin interfaz gráfica de usuario).

<div align="center">
<img src="/images/andronix-desktop.jpeg" width="33%" />
</div>

En el caso de seleccionar la primer opción tendriamos que escojer entre los 3 entornos de escritorio soporta para la mayoría de distribuciones disponibles en _Andronix_, estos son: _XFCE_, _LXQt_ y _LXDE_, todos estos entornos de escritorio ligeros.

<div align="center">
<img src="/images/andronix-desktop2.jpeg" width="33%" />
</div>

Una vez seleccionado el entorno de escritorio, automáticamente se copia el comando requerido en _Termux_ para la instalación de la distribución seleccionada. Se debe entonces abrir Termux y copiar el comando ( pulsando mas de 2 segundos y seleccionar la opción `Paste` ).

<div align="center">
<img src="/images/termux-paste.jpeg" width="33%" />
</div>

Cuando de haya pegado el comando pulsar `enter`.

<div align="center">
<img src="/images/termux-enter.jpeg" width="33%" />
</div  

Se iniciará un proceso de instalación que durará varios minutos en dependencia de las opciones seleccionadas, durante esta es posible que aparezca el mensaje de confirmación de las opciones de instalación de algún componente, generalmente `[default=N]`, a lo cual responderemos simplemente pulsando en `enter` para continuar con la opción por defecto.

### Contraseña del VNC

En la parte final de la instalación, se va a pedir especificar una contraseña de acceso para el entorno de escritorio por medio del servidor VNC.

<div align="center">
<img src="/images/termux-password.jpeg" width="33%" />
</div  

Debemos digitar una contraseña y pulsar `enter`.

Se nos puede pedir de nuevo una contraseña con el mensaje `view-only password [Y/N]` a lo cual contestamos `y` y luego `enter`, e ingresamos de nuevo la misma contraseña.

Es muy importante no olvidar esta contraseña ya que sin esta no podemos ingresar al entorno de escritorio de nuestra distribución.


### Fin de la instalación

Una vez finalice el proceso de instalación, se debe visalizar en la consola de comandos `root@localhost` para asegurarnos que ya estamos dentro de la distribución. Para salir de la distribución podemos digital el comando `exit`.

<div align="center">
<img src="/images/termux-setup-end.jpeg" width="33%" />
</div  

Desde _Termux_, ya instalada la distribución, se habrán creado un sistema de archivos y los scripts necesarios para el funcionamiento de la distribución instalada. Podemos comprobarlos digitando `ls` desde la carpeta home `'~'`. En la siguiente imagen se muestran los scripts resultantes de instalar dos distribuciones diferentes en _Andronix_: _Debian_ con escritorio _LXDE_ y _Manjaro_ con manejador de ventanas _Openbox_.

<div align="center">
<img src="/images/termux-ls.jpeg" width="33%" />
</div  


## Inicialización de la distribución sobre Termux

Una vez instalada podemos ejecutar la distribución ejecutando en termux el comando:

```
./start_<nombreDistribucion>.sh
```
en el caso de _Debian_:

```
./start_debian.sh
```
De nuevo, se debe visalizar en la consola de comandos `root@localhost` para asegurarnos que ya estamos dentro de la distribución.

<div align="center">
<img src="/images/start_debian.jpeg" width="33%" />
</div  

Para salir de la distribución y volver a la interfáz de comandos de _Termux_, basta con digitar `exit`.

## VNC Viewer

_VNC Viewer_ es una aplicación VNC (Virtual Network Computing), la cual permite mostrar la interfáz gráfica de un computador en otro diferente, ambos conectados a la misma red de datos. En este caso se visualizará por medio de _VNC Viewer_ la interfáz de la distribución instalada sobre _Termux_, sin tener que instalar ningún otro complemento gráfico. _VNC Viewer_ se puede descargar del _Google Play Store_.

## Configuración del VNC

Debemos modificar el archivo de inicio del servidor VNC para poder ejecutar el entorno de escritorio de la distribución, para esto vamos a instalar sobre nuestra distribución el editor de texto `nano`, con el siguiente comando:

```
apt install nano
```

Una vez instalado `nano` procedemos a abrir el script `/usr/local/bin/vncserver-start`

```
nano /usr/local/bin/vncserver-start
```

<div align="center">
<img src="/images/termux-nano.jpeg" width="33%" />
</div  

Vamos a modificar el script agragandole la geometría de pantalla deseada, en este caso y por comodidad de uso vamos a configurar la misma resolución de pantalla de nuestro celular, en este caso 720x1520. El entorno de escritorio se visulizará de forma vertical y no horizontal como lo haría en una pantalla normal de PC.

```
vncserver -name remote-desktop -geometry 720x1520 -localhost no :1
```

Una vez modificado el archivo se cuarda `CTRL o` y se cierra `CTRL x`.

## Inicialización del VNC

Estando en la distribución podemos ejecutar el script `/usr/local/bin/vncserver-start` para inicializar el servidor VNC, con el siguiente comando:

```
vncserver-start
```

<div align="center">
<img src="/images/vncserver-start.jpeg" width="33%" />
</div

Despues abrimos _VNC Viewer_ y pulsamos en el icono de **+**. Creamos una nueva sesión de VNP definiendo una dirección y un nombre.

<div align="center">
<img src="/images/vnc-new.jpeg" width="33%" />
</div

```
Address  = localhost:1
Name     = nombre_deseado
```

La direccion _Address_ debe coincidir con la definida el el script `/usr/local/bin/vncserver-start`.

Despues de ejecutar el comando `vncserver-start` y creada la sesión en _VNC Viewer_, podemos darle `CONNECT` e ingresar la contraseña definida al final del proceso de instalación de la distribución.

<div align="center">
<img src="/images/vnc-connect.jpeg" width="33%" />
</div

Se desplegará entonces el entorno de escritorio seleccionado. En el caso de este ejemplo será _LXDE_.

<div align="center">
<img src="/images/debian-ini.jpeg" width="33%" />
<img src="/images/debian-ini2.jpeg" width="33%" />
</div

Si queremos salir de la distribución le damos `DISCONNECT` en _VNC Viewer_ y ejecutamos los siguientes comandos en _Termux_:

```
vncserver-stop
exit
```

## Distribución de ejemplo

A modo de ejemplo se muestra la instalación y configuración de una distribución desde cero en: [Distribución de ejemplo](Distribución_ejemplo.md).

## Herramientas usadas

Para el desarrollo de esta guía si hizo uso de varios programas de software libre, el listado completo se puede ver en: [Herramientas usadas](Herramientas.md)

# Referencias
- [How to install an OS with Andronix? ](https://docs.andronix.app/unmodded-distros/unmodded-os-installation)
- [ VNC Basics](https://docs.andronix.app/vnc/vnc-basics/)
